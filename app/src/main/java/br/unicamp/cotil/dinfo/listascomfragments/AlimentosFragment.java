package br.unicamp.cotil.dinfo.listascomfragments;

import android.app.Activity;
import android.app.ListFragment;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Created by Prof. Matioli on 28/05/2017.
 */

public class AlimentosFragment extends ListFragment {
    private ArrayAdapter<Alimento> adapter;
    private OnClickItem listener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if(!(activity instanceof OnClickItem)){
           throw new RuntimeException("A classe não implementa a Interfade AlimentosFragment.OnClickItem!");
        }

        listener = (OnClickItem) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1);

        String[] nomes = getActivity().getResources().getStringArray(R.array.alimentos_nomes);
        String[] precos = getActivity().getResources().getStringArray(R.array.alimentos_precos);

        for (int i = 0; i < nomes.length; i++) {
            Alimento alimento = new Alimento(nomes[i], Double.parseDouble(precos[i]));
            adapter.add(alimento);
        }

        setListAdapter(adapter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Alimento alimento = adapter.getItem(position);
        listener.onClick(alimento);
    }

    public interface OnClickItem{
        public void onClick(Alimento alimento);
    }


}
