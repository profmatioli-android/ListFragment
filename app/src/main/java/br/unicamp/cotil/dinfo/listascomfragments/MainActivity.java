package br.unicamp.cotil.dinfo.listascomfragments;

import android.app.Activity;
import android.os.Bundle;

import java.text.NumberFormat;
import java.util.Locale;

public class MainActivity extends Activity implements AlimentosFragment.OnClickItem {
    private PrecoFragment precoFragment;
    private static final NumberFormat nf = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        precoFragment = (PrecoFragment) getFragmentManager().findFragmentById(R.id.frag_preco);

    }

    @Override
    public void onClick(Alimento alimento) {
        String texto;
        texto = String.format("O preco do(a) %s é %s ",alimento.getNome(), nf.format(alimento.getPreco()));
        precoFragment.exibePreco(texto);
    }
}
