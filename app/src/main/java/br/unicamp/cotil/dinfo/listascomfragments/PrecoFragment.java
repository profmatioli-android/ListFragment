package br.unicamp.cotil.dinfo.listascomfragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * Created by Prof. Matioli on 28/05/2017.
 */

public class PrecoFragment extends Fragment {
    private TextView txtPreco;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_preco, container, false);

        txtPreco = (TextView) view.findViewById(R.id.txt_preco);

        return view;
    }

    public void exibePreco(String text) {
        txtPreco.setText(text);
    }
}
